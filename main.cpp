#include <iostream>
#include <gl\glut.h>
#include <gl\glaux.h>
#include <time.h>
#include <math.h>

float cx=0, cy=0,bx = 2,by = 2;

int counter[4] = {1,1,1,1};
int b = 0;
void key(unsigned char key, int x, int y)
{
    switch (key)
    {
            case 'd':
                cx+=0.5f;
                break;
            case 'a':
                cx-=0.5f;
                break;
            case 'w':
                cy+=0.5f;
                break;
            case 's':
                cy-=0.5f;
                break;
    }
}
void display() {
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1,0,0);
	glTranslatef(cx + 0.92, cy + 0.92, 0.0);
	glBegin(GL_QUADS);
        glVertex3f(-0.05, 0.05, 0.0);  // ����� ������
        glVertex3f( 0.05, 0.05, 0.0);  // ������ ������
        glVertex3f( 0.05,-0.05, 0.0);  // ������ �����
        glVertex3f(-0.05,-0.05, 0.0);  // ����� �����
	glEnd();
	glFlush();
	glutPostRedisplay();
}

int main(int argc, char **argv) {


    // �������������
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(1000,600);
    glutCreateWindow("Game");
    // �������
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glClearColor(1,1,1,1);
    // �������� ���� GLUT
    glutMainLoop();

    return 1;
}
